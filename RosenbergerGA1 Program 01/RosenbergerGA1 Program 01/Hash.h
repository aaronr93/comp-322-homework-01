// Aaron Rosenberger - COMP 322
// 9/18/2015
// Hash.h
// Contains hash class implementation including find, insert, grow, etc.

#include <string>
#include <vector>
using std::vector;
using std::string;

template <class K, class Dat, class Fnc>
class Hash {
public:
	Hash() {
		// Set some default values
		loadFactor = 2;
		tableSize = 17;
		// Assigns an empty vector<Node> to tableSize slots in the table.
		table.assign(tableSize, vector<Node>(0));
		numberOfKeys = 0;
	}

	Hash(const float& load_factor) {
		loadFactor = load_factor;
		tableSize = 17;
		table.assign(tableSize, vector<Node>(0));
		numberOfKeys = 0;
	}
	
	Dat find(const K& key) {
		for (int i = 0; i < table.size(); i++) {
			// i loops through the vector of vectors. table[i] is of type vector<Node>.
			if (table[i].size()) /* If the spot in the hash table has at least one value */ {
				for (int j = 0; j < table[i].size(); j++) {
					// j loops through the vector of Nodes (chaining) for each spot in the hash table. 
					if (table[i][j].key == key) /* If the current key being examined is what we're looking for */ {
						return table[i][j].data;
					}
				}
			}
		}
	}
	
	void insert(const K& key, const Dat& data) {
		// Check the load factor before inserting
		if (numberOfKeys > 0 && tableSize / numberOfKeys < loadFactor) {
			tableSize *= 2;
			grow(tableSize);
		}
		// Get hashed key and make a new node for inserting.
		int hashedKey = hash(key) % tableSize;
		Node newNode(key, data);
		table.at(hashedKey).push_back(newNode);
		numberOfKeys++;
	}
	
private:
	struct Node {
		Node() {}
		Node(K newKey, Dat newData): key(newKey), data(newData) {}
		K key;
		Dat data;
	};

	void grow(const int& newTableSize) {
		// We need a temporary hash table to make sure everything's rehashed.
		vector< vector<Node> > oldTable = table;
		// Now that the old table is saved, resize the real table.
		table.resize(newTableSize);
		// Keep track of how many are rehashed so we don't have to loop through all of oldTable.
		int numRehashed = 0;

		for (int i = 0; i < oldTable.size(); i++) {
			// i loops through the old table. oldTable[i] is of type vector<Node>.
			if (oldTable[i].size()) /* If at least one value exists at oldTable[i] */ {
				for (int j = 0; j < oldTable[i].size(); j++) {
					// j loops through the chaining for each spot in the hash table. oldTable[j] is of type Node.
					insert(oldTable[i][j].key, oldTable[i][j].data);	// Rehash.
					numRehashed++;
				}
			}
		}
		
		numberOfKeys = numRehashed;
	}

	float loadFactor;
	int tableSize, numberOfKeys;
	vector< vector<Node> > table;
	Fnc hash;
};