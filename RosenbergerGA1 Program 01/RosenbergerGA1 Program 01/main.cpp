// Aaron Rosenberger - COMP 322
// 9/18/2015
// main.cpp
// Contains driver for hash class implementation
// Also contains functors for hash functions passed into hash class

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "Hash.h"
using std::string;
using std::hash;
using std::cout;
using std::endl;

struct floatHash {
	// The float will be cast to an int in order to perform
	// non-fuzzy hashing. Otherwise couldn't be sure of the key.
	int operator()(const float& x) {
		return abs((int)x);
	}
};

struct intHash {
	int operator()(const int& x) {
		return abs(x);
	}
};

struct stringHash {
	int operator()(string x) {
		hash<string> stringHash;
		return abs((int)stringHash(x));
	}
};

size_t getRandomNumber(const int& max, const string& type) {
	if (type == "int")
		return rand() % max;
	if (type == "float")
		return static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / max));
}

int main() {
	float loadFactor = 2.5;
	string testString = "Test string";
	string retval;
	srand(time(NULL));

	int max = 127;
	int randInt = getRandomNumber(max, "int");
	float randFloat = getRandomNumber(max, "float");


	Hash<float, string, floatHash> floatHashTable(loadFactor);

	floatHashTable.insert(randFloat, testString);
	floatHashTable.insert(randFloat, testString);
	floatHashTable.insert(randFloat, testString);

	randFloat = getRandomNumber(max, "float");
	floatHashTable.insert(randFloat, testString);

	randFloat = getRandomNumber(max, "float");
	floatHashTable.insert(randFloat, testString);

	randFloat = getRandomNumber(max, "float");
	floatHashTable.insert(randFloat, testString);

	randFloat = getRandomNumber(max, "float");
	floatHashTable.insert(randFloat, testString);

	retval = floatHashTable.find(randFloat);
	cout << retval << endl;


	Hash<int, string, intHash> intHashTable(loadFactor);

	intHashTable.insert(randInt, testString);
	intHashTable.insert(randInt, testString);
	intHashTable.insert(randInt, testString);

	randInt = getRandomNumber(max, "int");
	intHashTable.insert(randInt, testString);

	randInt = getRandomNumber(max, "int");
	intHashTable.insert(randInt, testString);

	randInt = getRandomNumber(max, "int");
	intHashTable.insert(randInt, testString);

	randInt = getRandomNumber(max, "int");
	intHashTable.insert(randInt, testString);

	retval = intHashTable.find(randInt);
	cout << retval << endl;


	Hash<string, int, stringHash> stringHashTable(loadFactor);

	stringHashTable.insert(testString, randInt);
	stringHashTable.insert(testString, randInt);
	stringHashTable.insert(testString, randInt);

	randInt = getRandomNumber(max, "int");
	stringHashTable.insert("A different string", randInt);

	randInt = getRandomNumber(max, "int");
	stringHashTable.insert("Again", randInt);

	randInt = getRandomNumber(max, "int");
	stringHashTable.insert("There once was a boy who could come up with clever random strings.", randInt);

	randInt = getRandomNumber(max, "int");
	stringHashTable.insert("He died.", randInt);

	retval = stringHashTable.find("Again");
	cout << retval << endl;

	return 0;
}